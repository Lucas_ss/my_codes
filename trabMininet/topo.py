#!/usr/bin/python   
#coding=UTF-8                                                                         
                                                                                        
from mininet.topo import Topo
from mininet.net import Mininet
from mininet.util import dumpNodeConnections
from mininet.cli import CLI
from mininet.node import RemoteController

class MyTopo(Topo):
    def __init__(self,**opts):
        # Initialize topology and default options
        Topo.__init__(self, **opts)

        #Adiciona o switch 's1' a topologia.
        for s in xrange(3):
            switch = self.addSwitch('s%d' % (s+1))

        #Adiciona hosts h1 e h2 a topologia.
        for h in xrange(3):
            host = self.addHost('h%d' % (h + 1))

        #Adiciona Links entre h1,s1 e h2.
        self.addLink('h1','s1')
        self.addLink('h2','s2')
        self.addLink('h3','s3')

        self.addLink('s1','s3')
        self.addLink('s1','s2')
        self.addLink('s2','s3')


def main():
    topo = MyTopo()
    net = Mininet(topo = topo, controller = RemoteController) 
    net.start()

    s1 = net.get('s1') #Get switch s1 instance from net
    
    #Adicionando no switch os fluxos via 'ovs-ofctl add-flow'
    # formato do comando:
    # ovs-ofctl add-flow [switch] [regras], [ações]
    #s1.cmd('ovs-ofctl add-flow s1 in_port=1,actions=output:2') 
    #s1.cmd('ovs-ofctl add-flow s1 in_port=2,actions=output:1')
    "Descomentando as linhas 36 e 37, os hosts devem ser capazes de enviarem pacotes um pro outro,\
    vocês podem testar usando ping, utilizando o comando h1 ping h2 na CLI do mininet" 

    print "Testing network connectivity"
    net.pingAll()


    CLI(net)
    net.stop()

if __name__ == '__main__':
    main()