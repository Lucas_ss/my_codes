#Trabalho Final -- MATA59-2018.1 - Redes de Computadores I 

##Instruções:

###Mininet:

Vocês podem baixar a vm do mininet nos seguintes links: [64 Bits](https://github.com/mininet/mininet/releases/download/2.2.2/mininet-2.2.2-170321-ubuntu-14.04.4-server-amd64.zip)
ou [32 Bits](https://github.com/mininet/mininet/releases/download/2.2.2/mininet-2.2.2-170321-ubuntu-14.04.4-server-i386.zip).

Também existem outras opções de instalação do mininet: [Tutorial](http://mininet.org/download/).

###Topologia:

![topologia](/images/topo.jpeg)

O arquivo [topo.py](topo.py) é um exemplo de script python que cria um topologia simples com 1 switch e 2 hosts usando a API do mininet pro python, os alunos podem se basear nesse script para criar suas proprias topologias. O codigo está comentado, indicando como usar os metodos mais importantes. Para executar o script basta rodar o comando abaixo:

```
$ sudo python topo.py
```
No script também contem instruções basicas sobre como enviar regras diretamente pro switch usando ovs-ofctl o formato do comando que deve ser executado na CLI do mininet é:
```
mininet> sh ovs-ofctl add-flow [switch] [regras], [ações]
```
também é possivel executar esses comandos no proprio script de criação da topologia, como está descrito nos comentários do codigo de exemplo. É importante lembrar que mesmo essa sendo uma forma de instalar regras diretamente no switch, no trabalho vocês devem usar o controlador para instalar essas regras nos switches de forma reativa.

###Controlador:

Irei deixar um tutorial simples sobre o POX, mas vocês estão livres para usarem o controlador que decidirem, o [Ryu](https://github.com/osrg/ryu) também é uma boa opção de controlador e possui uma boa [documentação](https://ryu.readthedocs.io/en/latest/) na internet. O pox já está dentro do repositório do trabalho final.
```
$ cd pox
```
Para executar o controlador basta executar o comando:
```
$ ./pox.py simpleApp.pox_simpleApp
```
Para mudar o comportamento do controlador, basta modificar o arquivo [pox_simpleApp.py](pox/pox/simpleApp/pox_simpleApp.py), o codigo está todo comentado explicando o que cada parte faz. Nessa aplicação, o controlador está programado para funcionar de forma reativa, assim como vocês devem fazer no trabalho final. Nessa aplicação ao receber um packet_in¹ do switch o controlador apenas checa se o pacote está vindo da porta 1 ou da porta 2, caso esteja vindo da porta 1 ele cria uma regra de encaminhamento no switch para encaminhar todos os pacotes vindos da porta 1 para 2, caso esteja vindo da porta 2, ele cria uma regra no switch para encaminhar todos os pacotes vindos da porta 2 para 1. Vocês devem modificar esse codigo de forma que as regras tornem possiveis o funcionamento da rede da topologia de vocês, é possivel fazer isso de forma estática, porém também é possivel usar abordagens mais elegantes e fazer aplicações que vão fazer a rede funcionar para qualquer topologia, a estrategia que vocês vão usar fica a encargo da criatividade de vocês. 

Dentro da pasta [simpleApp](pox/pox/simpleApp) também tem um outro codigo de exemplo: o [hub.py](pox/pox/simpleApp/hub.py), do proprio POX, que faz regras para que cada switch aja como um hub. 

Aqui está a [Documentação do POX](https://noxrepo.github.io/pox-doc/html/). Em caso de duvidas, sintam-se a vontade para me mandar emails. tentarei responder o mais rapido possivel. Meu e-mail: [lucassouzasantos@ufba.br](mailto:lucassouzasantos@ufba.br)





1: Quando um switch OpenFlow recebe um pacote para o qual ele não tem uma regra definida, ele envia esse pacote ao controlador, para que o controlador possa tomar as ações necessarias e/ou instanciar as regras necessarias nos switches, esse evento é chamado de packet_in.

