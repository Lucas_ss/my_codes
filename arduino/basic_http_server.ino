#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>

const char* ssid     = "asdasd";
const char* password = "";

IPAddress apIP(192, 168, 1, 1);        //FOR AP mode
IPAddress netMsk(255,255,255,0);         //FOR AP mode

const char* html = "<html> </html>";

ESP8266WebServer server(80);

void setup() {
  
  Serial.begin(115200);

//***************WIFI client************************//
  /*WiFi.begin(ssid, password);
  Serial.print("\n\r \n\rWorking to connect");
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());*/
//***************WIFI ACCESS POINT******************//
  WiFi.mode(WIFI_AP);
  WiFi.softAPConfig(apIP,apIP,netMsk);
  WiFi.softAP(ssid,password);  //leave password away for open AP
  Serial.print("SSID: ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.softAPIP());
//**************************************************//

  server.on("/", handle_index);             // Seta a pagina index
  server.on("/getUID",handle_GetUID);       // Seta a pagina que devolve o UID
  server.onNotFound(handleNotFound);        //page if not found
  server.begin();
  Serial.println("HTTP server started");
  //wdt_disable();
}

void loop(){
  //Serial.println("Hu3");
  server.handleClient();
  //wdt_disable();
  delay(50);

}

void handleNotFound() {
  Serial.print("\t\t\t\t URI Not Found: ");
  Serial.println(server.uri());
  server.send ( 200,"text/plain","URI Not Found" );
}

void handle_index() {
  String toSend = "<form><p><input type=\"submit\" value=\"OK\"></p></form>";
  server.send(200, "text/html", toSend);
  delay(100);
}


///////////TRABALHAR NESSA FUNÇÂO !! ///////////////////
void handle_GetUID(){
  //While(!RecebeUID && i < 100) { UID = getUID(); i++ ; delay(100); }

  String UID = "Hu3";
  String toSend = UID; // pegar o UID Previamente
  server.send(200,"text/plain",UID + "\0");
  delay(100);
}
