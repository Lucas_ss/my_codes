
double C = 0;

class DNA{
  public:
    int genes[3];
    DNA(){
      for(int i=0; i<sizeof(genes)/sizeof(int); i++){
        randomSeed(analogRead(A0));
        genes[i] = random(100);  
      }
    }
    int getGenesSize(){
      return sizeof(genes)/sizeof(int);
    }
};

class Individuo{
  private:
    DNA dna;

  public:
    Individuo(){
      this->dna = *(new DNA());
    }

    int* getGenes(){
      return this->dna.genes;
    }
  
    Individuo crossover(Individuo otherParent){
      Individuo child = *(new Individuo());
      for(int i=0; i< child.dna.getGenesSize(); i++){
        randomSeed(analogRead(A0));
        int dado = random(2);
        if(dado == 0)
          child.getGenes()[i] = this->getGenes()[i];
        else
          child.getGenes()[i] = otherParent.getGenes()[i];
      }
    }

    double fitness(double atualTemp, long t){
      int* g = this->dna.genes;
      double estimatedTemp = g[0]*(t*t*t)+ g[1]*(t*t) + g[2]*t + C;      
      double value = estimatedTemp/atualTemp;
      value = mod(value);
      int offset = value;
      double fit = value - offset;
      
      return fit;
    }
    
    double mod(double value){
      if( value <0)
        value = value*(-1);

      return value;
    }
};




Individuo pop[100];


Individuo bestFit(){
  double bestFit = -1;
  int idx;
  long t = millis();
  double atualTemp = 0; // COLOCAR O SENSOR AQUI
  for(int i=0; i<sizeof(pop)/sizeof(Individuo);i++){
    double iFit =pop[i].fitness(atualTemp,t);
    if(iFit > bestFit){
      bestFit = iFit;
      idx = i;
    }
  }
  return pop[idx];
}

void setup() {
  // put your setup code here, to run once:

}

void loop() {
  // put your main code here, to run repeatedly:

}
