#include <iostream>
#include <stack>
using namespace std;
int QWE = 1;
string guloso(string numero, int toErase, int maxSize){
    stack<int> pilha;

    for(int i=0; i<numero.size(); i++){
        int num = numero[i] - '0';
        while(!pilha.empty() && num > pilha.top() && toErase > 0){
            pilha.pop();
            toErase--;
        }
        if(pilha.size() < maxSize)
            pilha.push(num);
    }
    string erasedNum;
    for(int i=0; i< maxSize; i++)
        erasedNum += " ";
    while(!pilha.empty()){
        maxSize--;
        erasedNum[maxSize] = pilha.top() + '0';
        pilha.pop();
    }
    return erasedNum;
}

int main(){
    int n,d;
    string numero;
    cin>>n>>d;
    while(n != 0 || d != 0){
        cin>>numero;
        cout<<guloso(numero,d,n-d)<<"\n";
        cin>>n>>d;
    }
}

