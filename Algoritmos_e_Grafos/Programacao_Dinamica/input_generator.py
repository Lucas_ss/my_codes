import random
import os
MAX_N = 5
MAX_K = 5
NUM_CASES = 300

def main():
    data = ""
    data += str(NUM_CASES) + "\n"
    for t in xrange(NUM_CASES):
        random.seed(os.urandom(10))
        n = random.randint(1,MAX_N)
        k = random.randint(1,MAX_K)
        pos = []
        v = []
        for i in xrange(n):
            pos.append(random.randint(0,50))

        pos.sort()
        for i in xrange(n):
            v.append(random.randint(0,50))

        data += str(n) + " " + str(k) + "\n"

        for i in pos:
            data += str(i)+ " "
        data+='\n'

        for i in v:
            data += str(i)+ " "
        data+='\n'

    f = open("input.txt",'w')
    f.write(data)


if __name__ == '__main__':
    main()