

def max_value(a,b,c):
    if a>=b and a>=c:
        return a
    if b>=c:
        return b
    return c

def max_lucro(pos,lucro,n,k,mem):
    if n==0:
        return 0
    if mem[n] >=0:
        return mem[n]
    
    cont=0
    distFromK = k
    while n-cont >0 and distFromK >0:
        distFromK -= pos[n] - pos[n-cont]
        cont+=1
    
    nextIdx = n-cont
    aux = -1
    if distFromK <= 0:
        aux = max_value(aux,lucro[n]+max_lucro(pos,lucro,nextIdx,k,mem),max_lucro(pos,lucro,n-1,k,mem))
    else:
        aux = max_value(aux,lucro[n],max_lucro(pos,lucro,n-1,k,mem))

    mem[n] = aux
    return aux

def main():
    t = int(raw_input())
    for i in xrange(t):
        n = int(raw_input())
        k = int(raw_input())
        mem = []
        pos = []
        lucro = []
        for j in xrange(n):
            pos[j+1] = int(raw_input())
        
        for j in xrange(n):
            pos[j+1] = int(raw_input())

        for j in xrange(n):
            mem[j+1] = -1

        print max_lucro(pos,lucro,n,k,mem)
    
if __name__ == '__main__':
    main()