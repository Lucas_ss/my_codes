#include <iostream>
using namespace std;
int mem[1000][1000];

int mochila(int* p,int* v,int n,int c){
    if(n == 0 || c == 0)
        return 0;
    if(mem[n-1][c-1] != -1)
        return mem[n-1][c-1];
    
    int aux;
    if( p[n-1] > c )
        aux = mochila(p,v,n-1,c);
    else
        aux = max(mochila(p,v,n-1,c),v[n-1] + mochila(p,v,n-1,c-p[n-1]));
    
    //cout<<"F("<<n<<","<<c<<") = "<<aux<<"\n";
    mem[n-1][c-1] = aux;
    return aux;
}



int main(){
    int n,c,test=1;
    cin>>n>>c;
    while(n != 0 || c != 0){
        int p[n],v[n];
        for(int i=0; i<n; i++)
            cin>>p[i]>>v[i];
        for(int i=0; i<n; i++)
            for(int j=0; j<c; j++)
                mem[i][j] = -1;



        cout<<"Caso "<<test++<<": "<<mochila(p,v,n,c)<<"\n";
        cin>>n>>c;
    }
}