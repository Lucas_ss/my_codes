#include <iostream>
using namespace std;
int mem[1000][1000];

int max_value(int a, int b, int c){
    if( a >= b && a >= c)
        return a;
    if( b >= c)
        return b;
    return c;
}

int max_lucro(int* pos,int* lucro, int n, int k, int remainDistance){
    if(n == 0)
        return 0;
    if(mem[n][remainDistance] >= 0 ) // Lembrar de inicializar mem com -1
        return mem[n][remainDistance];

    int aux=-1;
    if( pos[n-1] +k <= remainDistance)
        aux = max_value(aux,lucro[n-1]+max_lucro(pos,lucro,n-1,k,pos[n-1]),max_lucro(pos,lucro,n-1,k,remainDistance));
    else
        aux = max_value(aux,max_lucro(pos,lucro,n-1,k,remainDistance),-1);
    
    mem[n][remainDistance] = aux;
    return aux;
}

int main(){
    int t,n,k;
    cin>>t;
    for(int i=0; i<t; i++){
        cin>>n>>k;
        // int mem[n*n];
        for(int j=0;j<n;j++)
            for(int k=0;k<n;k++)
                mem[j][k] = -1;
        
        int pos[n],lucro[n];
        for(int j=0; j<n; j++)
            cin>>pos[j];

        for(int j=0; j<n; j++)
            cin>>lucro[j];
    
    
    int res = max_lucro(pos,lucro,n,k,pos[n-1]+k);
    cout<<res<<"\n";

    }
}
