#include <iostream>
using namespace std;

long max_value(long a, long b, long c){
    if( a >= b && a >= c)
        return a;
    if( b >= c)
        return b;
    return c;
}

long max_lucro(long* pos,long* lucro, long n, long k,long* mem){
    if(n == 0)
        return 0;
    if(mem[n] >= 0 )
        return mem[n];
    
    long nextIdx = -1;
    for(int i=n-1; i>0; i--){
        if(pos[n] - pos[i] >=k){
            nextIdx = i;
            break;
        }
    }
    long aux=-1;
    //cout<<"F("<<n<<") fork->";
    if( nextIdx > -1){
        //cout<<"F("<<nextIdx<<")+v["<<n<<"] & F("<<n-1<<")\n"; 
        aux = max_value(aux,lucro[n]+max_lucro(pos,lucro,nextIdx,k,mem),max_lucro(pos,lucro,n-1,k,mem));
    }
    else{
        //cout<<"F("<<n-1<<")+v["<<n<<"]\n";
        aux = max_value(aux,max_lucro(pos,lucro,n-1,k,mem),lucro[n]);
    }
    mem[n] = aux;
    return aux;
}

int main(){
    long t,n,k;
    cin>>t;
    for(long i=0; i<t; i++){
        cin>>n>>k;
        long mem[n+1];
        
        long pos[n+1],lucro[n+1];
        for(long j=1; j<=n; j++)
            cin>>pos[j];

        for(long j=1; j<=n; j++)
            cin>>lucro[j];
    
        for(long j=1;j<=n;j++)
            mem[j]= -1;

        long res = max_lucro(pos,lucro,n,k,mem);
        cout<<res<<"\n";
    }
}