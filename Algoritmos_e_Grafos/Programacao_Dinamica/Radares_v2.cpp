#include <iostream>
using namespace std;

int max_value(int a, int b, int c){
    if( a >= b && a >= c)
        return a;
    if( b >= c)
        return b;
    return c;
}

int max_lucro(int* pos,int* lucro, int n, int k,int* mem){
    if(n == 0)
        return 0;
    if(mem[n-1] >= 0 ) // Lembrar de inicializar mem com -1
        return mem[n-1];
    
    int count=0,distFromK = k;
    while( n-count >0 && distFromK > 0 ){
        distFromK -= (pos[n-1] - pos[n-1-count]);
        count++;
    }

    int nextIdx = n-count+1;
    int aux=-1;

    if( distFromK <= 0)
        aux = max_value(aux,lucro[n-1]+max_lucro(pos,lucro,nextIdx,k,mem),max_lucro(pos,lucro,n-1,k,mem));
    else
        aux = max_value(aux,max_lucro(pos,lucro,n-1,k,mem),lucro[n-1]);

    mem[n-1] = aux;
    return aux;
}

int main(){
    int t,n,k;
    cin>>t;
    for(int i=0; i<t; i++){
        cin>>n>>k;
        int mem[n];
        
        int pos[n],lucro[n];
        for(int j=0; j<n; j++)
            cin>>pos[j];

        for(int j=0; j<n; j++)
            cin>>lucro[j];
    
        for(int j=0;j<n;j++)
            mem[j]= -1;

        int res = max_lucro(pos,lucro,n,k,mem);
        cout<<res<<"\n";
    }
}