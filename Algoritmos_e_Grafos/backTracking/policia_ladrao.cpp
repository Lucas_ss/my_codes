#include <iostream>
using namespace std;

bool mat[5][5];


bool back_track(int x,int y){
    if(mat[x][y] == 1)
        return false;
    mat[x][y] = 1;
    bool r = false;
    for (int i = -1; i<2; i++)
            if(x + i >=0 && x+i<5 ){
                if(x+i == 4 && y == 4)
                        return true;    
                if(mat[x+i][y] == 0){ 
                    r = back_track(x+i,y);
                    if(r)
                        return r;
                }
            }
    for (int i = -1; i<2; i++)
            if(y + i >=0 && y+i<5 ){
                if(x == 4 && y+i == 4)
                        return true;    
                if(mat[x][y+i] == 0){ 
                    r = back_track(x,y+i);
                    if(r)
                        return r;
                }
            }
    return r;
    
}

int main(){
    int t;
    cin>>t;
    for(int i=0; i<t; i++){
        for(int j=0; j<25; j++)
            cin>>mat[j%5][j/5];


        if(back_track(0,0))
            cout<<"COPS\n";
        else    
            cout<<"ROBBERS\n";

    }
} 