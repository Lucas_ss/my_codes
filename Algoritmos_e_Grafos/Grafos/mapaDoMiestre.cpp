#include <iostream>
using namespace std;

int main(){
    int x,y;
    cin>>x>>y;
    char mapa[x][y];
    for(int j=0; j<y; j++){
        for(int i=0; i<x; i++)
            cin>>mapa[i][j];
    }
    int i=0,j=0; 
    char lastCommand;
    while(mapa[i][j] != '*' && mapa[i][j] != '!'){
        
        if(mapa[i][j] != '*' && mapa[i][j] != '.')
            lastCommand = mapa[i][j];
        mapa[i][j] = '!';

        if(lastCommand == '>')
            i++;
        else if(lastCommand == '<')
            i--;
        else if(lastCommand == '^')
            j--;
        else if(lastCommand == 'v')
            j++;          
    }
    cout<<mapa[i][j]<<'\n';
}