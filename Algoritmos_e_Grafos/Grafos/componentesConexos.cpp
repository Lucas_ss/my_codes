#include <iostream>
#include <queue>
#include <vector>
#include <algorithm>
using namespace std;

int main(){
    int t;
    cin>>t;
    for(int k=0; k<t; k++){
        int n,m;
        cin>>n>>m;
        int graph[n][n];
        char vertice[n];
        for(int i=0; i<n; i++)
            for(int j=0; j<n; j++)
                graph[i][j] = 0;
                
        for(int u=0; u<m; u++){
            char i,j;
            cin>>i>>j;
            graph[i - 'a'][j - 'a']=1;
            graph[j - 'a'][i - 'a']=1;
        }
        cout<<"Case #"<<k+1<<":\n";
        for(int i=0; i<n; i++)
            vertice[i] = 'B';
        int cont=0;
        for(int i=0; i<n; i++){
            vector<char> list;
            if(vertice[i] == 'B'){
                cont++;
                queue<int> aux_queue;
                aux_queue.push(i);
                vertice[i] = 'C';

                while( !aux_queue.empty() ){
                    int node = aux_queue.front();
                    list.push_back(((char)(node+'a')));
                    aux_queue.pop();
                    for(int i=0; i<n; i++){
                        if(graph[node][i] == 1){
                            if(vertice[i] == 'B'){
                                vertice[i] = 'C';
                                aux_queue.push(i);
                            }
                        }
                    }
                    vertice[node] = 'P';
                }
                std::sort(list.begin(),list.end());
                for(int i=0; i<list.size(); i++)
                    cout<<list[i]<<',';
                cout<<'\n';
            }
        }
        cout<<cont<<" connected components\n\n";
    }
}