#include <iostream>
#include <queue>
using namespace std;

typedef struct no_t no_t;
struct no_t{
    int nivel;
    int valor;
};

int invert(int n){
        int inverted,aux=10,maxAux=0;
        while(n/(aux/10) >0)
            aux*=10; 
        maxAux=aux/10;
        aux=10;
        inverted=0;
        while(n/(aux/10) >0){
            inverted +=  ((n%aux) - n%(aux/10))/(aux/10)*maxAux/aux;
            aux *=10;
    }
    return inverted;
}

int main(){
    int t,a,b;;
    cin>>t;
    for(int k=0; k<t; k++){
        queue<no_t> f;
        cin>>a>>b;
        
        bool vet[10000];
        for(int i=0; i<10000; i++)
            vet[i] = false; 

        no_t no; no.valor = a; no.nivel=0;
        f.push(no);
        int cont=0;
        while(!f.empty()){
            no_t lastNo = f.front();
            int lastNum = lastNo.valor;
            cont = lastNo.nivel;

            f.pop();
            if(lastNum == b)
                break;

            int inverted = invert(lastNum);
            int next = lastNum+1;
            no_t nextNo; nextNo.valor=next; nextNo.nivel=lastNo.nivel+1;
            no_t invertedNo; invertedNo.valor = inverted; invertedNo.nivel = lastNo.nivel+1; 

            if(next > 10000)
                f.push(nextNo);           
            
            else if( !vet[next] ){
                f.push(nextNo);
                vet[next] = true;
            }

            if(inverted > 10000)
                f.push(invertedNo);
            else if( !vet[inverted]) {
                f.push(invertedNo);
                vet[inverted] = true;
            }
        }
        cout<<cont<<'\n';
    }
}