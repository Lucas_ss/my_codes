#include <iostream>
#include <queue>
using namespace std;

int main(){
    int t;
    cin>>t;
    for(int k=0; k<t; k++){
        int n,m;
        cin>>n>>m;
        int graph[n][n];
        char vertice[n];
        for(int i=0; i<n; i++)
            for(int j=0; j<n; j++)
                graph[i][j] = 0;
                
        for(int u=0; u<m; u++){
            int i,j;
            cin>>i>>j;
            graph[i-1][j-1]=1;
            graph[j-1][i-1]=1;
        }

        for(int i=0; i<n; i++)
            vertice[i] = 'B';
        int cont=-1;
        for(int i=0; i<n; i++){
            if(vertice[i] == 'B'){
                cont++;
                queue<int> aux_queue;
                aux_queue.push(i);
                vertice[i] = 'C';

                while( !aux_queue.empty() ){
                    int node = aux_queue.front();
                    aux_queue.pop();
                    for(int i=0; i<n; i++){
                        if(graph[node][i] == 1){
                            if(vertice[i] == 'B'){
                                vertice[i] = 'C';
                                aux_queue.push(i);
                            }
                        }
                    }
                    vertice[node] = 'P';
                }
            }
        }
        cout<<"Caso #"<<k+1<<": ";
        if(cont == 0)
            cout<<"a promessa foi cumprida\n";
        else
            cout<<"ainda falta(m) "<<cont<<" estrada(s)\n";
    }
}