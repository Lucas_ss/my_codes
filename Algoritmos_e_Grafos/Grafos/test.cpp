#include <iostream>
using namespace std;
int invert(int n);

int main(){
    int n;
    cin>>n;
        int inverted = invert(n);
        cout<<inverted<<'\n';
        cin>>n;
}

int invert(int n){
        int inverted,aux=10,maxAux=0;
        while(n/(aux/10) >0)
            aux*=10;
        
        maxAux=aux/10;
        aux=10;
        inverted=0;
        while(n/(aux/10) >0){
            //cout<<"= "<<((n%aux)-n%(aux/10))<<"*("<<maxAux/aux<<")\n";
            inverted +=  ((n%aux) - n%(aux/10))/(aux/10)*maxAux/aux;
            aux *=10;
    }
    return inverted;
}