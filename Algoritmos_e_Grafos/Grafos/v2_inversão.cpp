#include <iostream>
#include <queue>
using namespace std;

int invert(int n){
        int inverted,aux=10,maxAux=0;
        while(n/(aux/10) >0)
            aux*=10;
        
        maxAux=aux/10;
        aux=10;
        inverted=0;
        while(n/(aux/10) >0){
            //cout<<"= "<<((n%aux)-n%(aux/10))<<"*("<<maxAux/aux<<")\n";
            inverted +=  ((n%aux) - n%(aux/10))/(aux/10)*maxAux/aux;
            aux *=10;
    }
    return inverted;
}

int main(){
    int t;
    cin >> t;
    for(int k=0; k<t; k++){
        queue<int> f;
        int a,b;
        unsigned cont=0,cont2=0;
        cin>>a>>b;

        int vet[b+2];
        for(int i=0; i<b+2; i++)
            vet[i] = 0;

        f.push(a);
        while(! f.empty()){
            int p=0;
            //cout<<"$$ "<<++p;
            int lastNum = f.front();
            //cout<<"@@ "<<cont2++<<": "<<lastNum<<" size: "<<f.size()<<" aux: "<<f_aux.size()<<"\n";

            //cout<<"$"<<++p<<" "; // 1
            //if(f.size() >1)
            cout<<f.size()<<"\n"; 
            if(f.size() == 0)
                cout<<"@@@@@@@@@@@@@@@@@@@@@@@@@@";
                f.pop();

            if(lastNum == b)
                break;
            //cout<<"$"<<++p<<" ";
            if(lastNum < b+1)
                vet[lastNum] = 1;

            int invertedNum = invert(lastNum);
            //cout<<"$"<<++p<<" ";
            if(lastNum > b || !vet[invertedNum]){
                f.push(invertedNum);
                if(invertedNum < b+1)
                    vet[invertedNum] = 1;
            }
            //cout<<"$"<<++p<<" ";
            if(lastNum > b || !vet[lastNum+1]){
                f.push(lastNum+1);
                if(lastNum+1 < b+1)
                    vet[lastNum+1] = 1;
            }
            //cout<<"$"<<++p<<" ";
            cont++;
            //cout<<"$"<<++p<<" "; // 6
        }
        cout<<cont<<"\n";
    }
}