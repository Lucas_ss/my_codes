import java.util.*;

public class Environment implements Visible{
private static Environment uniqueInstance;
    private List<Objeto> objetos;
    
    private Environment(){
        this.objetos = new ArrayList<Objeto>();
    }

    public Environment getInstance(){
        if( this.uniqueInstance == null )
            this.uniqueInstance = new Environment();
        
        return this.uniqueInstance;
    }

    public void addObjeto(Objeto obj){
        this.objetos.add(obj);
    }
        
    public void addObjeto(List<Objeto> objList){
        for(Objeto obj : objList)
            this.objetos.add(obj);
    }

    public void show(){
        for(Objeto obj : this.objetos)
            obj.show();
    }

}