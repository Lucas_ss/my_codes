public class Tela{
    private static Tela uniqueInstance;
    public processing.core.PApplet test;

    private Tela(){}

    public static Tela getInstance(processing.core.PApplet app){
        if( uniqueInstance == null )
            uniqueInstance = new Tela();
    
        return uniqueInstance;
    }

}